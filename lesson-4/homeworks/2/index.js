/**
 * Задача 2.
 *
 * Напишите скрипт, который проверяет идентичны ли массивы.
 * Если массивы полностью идентичны - в переменную flag присвоить значение true, 
 * иначе - false. 
 * 
 * Пример 1: const arr1 = [1, 2, 3];
 *           const arr2 = [1, '2', 3];
 *           flag -> false
 *
 * Пример 2: const arr1 = [1, 2, 3];
 *           const arr2 = [1, 2, 3];
 *           flag -> true
 *
 * Пример 3: const arr1 = [];
 *           const arr2 = arr1;
 *           flag -> true
 *
 * Пример 4: const arr1 = [];
 *           const arr2 = [];
 *           flag -> true
 * 
 * Условия:
 * - Обязательно проверять являются ли сравниваемые структуры массивами;
 *
*/

const arr1 = [1, 2, 3];
const arr2 = [1, "2", 3];
let flag = '';
let flag2 = '';

// РЕШЕНИЕ

flag = JSON.stringify(arr1) === JSON.stringify(arr2)

flag2 = arr1.every((el, i)=> el === arr2[i])

console.log(flag, flag2);